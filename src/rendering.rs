use stdweb;

pub trait Drawable {
    fn draw(&self, ctx: &stdweb::Value);
}
