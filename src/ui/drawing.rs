use ui::UI;
use world::Grid;
use world::tile;

pub fn pre(ui: &UI, grid: &mut Grid) {
    let mouse = ui.mouse.borrow();
    if !mouse.over {
        return;
    }
    // TODO: move this to world::tile or something
    const TILE_SIZE: i32 = 10;
    let tile_screen_size = TILE_SIZE * ui.scale();
    let x = mouse.scaled_x() / tile_screen_size;
    let y = mouse.scaled_y() / tile_screen_size;
    if mouse.buttons.left() {
        grid.set(
            x,
            y,
            Some(tile::Tile {
                style: ui.tile(),
                slope: ui.slope(),
                deadly: ui.deadly(),
                liquid: ui.liquid(),
                noclip: ui.noclip(),
                friction: ui.friction(),
            }),
        );
    } else if mouse.buttons.right() {
        grid.set(x, y, None);
    }
}
