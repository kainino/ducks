use stdweb::web::{document, IEventTarget};
use stdweb::web::INonElementParentNode;
use stdweb::web::event::ClickEvent;

pub fn init() {
    let connectbtn = document().get_element_by_id("connect").unwrap();
    connectbtn.add_event_listener(|e: ClickEvent| {
        js! {
            console.log(@{e});
        }
    });
}
