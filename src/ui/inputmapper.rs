use std::cell::RefCell;
use std::rc::Rc;
use stdweb::unstable::TryFrom;
use stdweb::web;
use stdweb::web::IEventTarget;
use stdweb::web::INonElementParentNode;
use stdweb::web::event::{IEvent, IKeyboardEvent, KeyDownEvent};
use super::inputmapping::{InputMapping, Action};

struct InputMapper {
    mapping: Rc<RefCell<InputMapping>>,
    container: web::Element,
}

impl InputMapper {
    fn new(mapping: Rc<RefCell<InputMapping>>) -> Rc<RefCell<InputMapper>> {
        let container = web::document().get_element_by_id("mappings").unwrap();
        Rc::new(RefCell::new(InputMapper {
            mapping,
            container,
        }))
    }
}

fn makeui(mapper: Rc<RefCell<InputMapper>>, action: Action, key: &str) {
    let mapping = mapper.borrow().mapping.clone();
    let container = &mapper.borrow().container;
    let doc = web::document();

    let e_key = web::document().create_element("input").unwrap();
    let e_key = web::html_element::InputElement::try_from(e_key).unwrap();
    {
        let elem = e_key.clone();
        e_key.add_event_listener(move |e: KeyDownEvent| {
            let code = e.code();
            // Ignore any browser keybindings (tab, shift-tab, ctrl-t, etc.)
            if e.alt_key() || e.ctrl_key() || e.meta_key() || e.shift_key() || code == "Tab" {
                return;
            }

            let mut mapping = mapping.borrow_mut();
            if code == "Backspace" {
                // clear binding
                elem.set_raw_value("");
                mapping.map.insert(action, "".into());
            } else if code != "Escape" {
                elem.set_raw_value(&code);
                mapping.map.insert(action, code);
            }
            e.prevent_default();
        });
    }

    js! {
        let container = @{container};
        let action = @{action.name()};
        let key = @{key};

        let ePair = document.createElement("div");
        ePair.className = "mappingpair";

        let eAction = document.createElement("label");
        ePair.appendChild(eAction);
        eAction.className = "mappingaction";
        eAction.htmlFor = "mappingkey-" + action;
        eAction.innerText = action + ":";

        let eKey = @{e_key};
        ePair.appendChild(eKey);
        eKey.className = "mappingkey";
        eKey.id = "mappingkey-" + action;
        eKey.type = "text";
        eKey.setAttribute("spellcheck", false);
        // TODO: would be nice to show the 'e.key' instead of 'e.code', but
        // I don't have a way to convert 'e.code' to 'e.key' here.
        eKey.value = key;

        container.appendChild(ePair);

        return eKey;
    };

    //this.fields.insert(action, e_key);
}

pub fn init(mapping: Rc<RefCell<InputMapping>>) {
    let mapper = InputMapper::new(mapping.clone());
    let mapping = mapping.borrow();

    for (&a, k) in &mapping.map {
        makeui(mapper.clone(), a, k);
    }

    // TODO
}
