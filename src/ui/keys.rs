use std::cell::RefCell;
use std::collections::HashSet;
use std::rc::Rc;

#[derive(Debug)]
pub struct Keys {
    pub key: HashSet<String>,
    pub new_key: HashSet<String>,
}

impl Keys {
    pub fn new() -> Rc<RefCell<Keys>> {
        let this = Rc::new(RefCell::new(Keys {
            key: HashSet::new(),
            new_key: HashSet::new(),
        }));
        let on_key_down = {
            let this = this.clone();
            move |code: String| {
                let mut this = this.borrow_mut();
                if !this.key.contains(&code) {
                    this.new_key.insert(code.clone());
                }
                this.key.insert(code);
            }
        };
        let on_key_up = {
            let this = this.clone();
            move |code: String| {
                this.borrow_mut().key.remove(&code);
            }
        };
        js! {
            document.onkeydown = e => {
                let ac = document.activeElement;
                // Ignore any browser/os keybindings (tab, shift-tab, ctrl-t...)
                if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey ||
                    e.code === "Meta" || e.code === "Tab") {
                    return true;
                }
                // For text inputs, ...
                if (ac.tagName == "INPUT" && ac.type == "text") {
                    // ... escape should deselect, ...
                    if (e.code === "Escape") {
                        document.activeElement.blur();
                    }
                    // ... and no keypresses should be stored.
                    return true;
                }

                @{on_key_down}(e.code);
            };
            document.onkeyup = e => @{on_key_up}(e.code);
        }
        this
    }

    pub fn pressed(&self, code: &str) -> bool {
        self.key.contains(code)
    }

    pub fn pressed_this_frame(&self, code: &str) -> bool {
        self.new_key.contains(code)
    }

    pub fn post(&mut self) {
        //let s = format!("{:?}", self);
        //js! { console.log(@{s}); }

        self.new_key.clear();
    }
}
