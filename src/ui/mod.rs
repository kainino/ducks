#![allow(dead_code)]
#![allow(unused_variables)]
mod connect;
mod multiselect;
mod mouse;
mod keys;
mod drawing;
mod inputmapping;
mod inputmapper;

use std::cell::RefCell;
use std::rc::Rc;
use std::fmt;
use stdweb;
use stdweb::web;
use stdweb::web::IEventTarget;
use stdweb::web::INonElementParentNode;
use stdweb::web::event::IEvent;
use stdweb::Value;
use stdweb::unstable::TryInto;

use rendering::Drawable;
use world::tile;
use world::Grid;
use self::multiselect::Multiselect;
use self::mouse::Mouse;
use self::keys::Keys;
use self::inputmapping::InputMapping;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Tool {
    Pencil,
    Rectangle,
}

pub struct UI {
    pub mouse: Rc<RefCell<Mouse>>,
    keys: Rc<RefCell<Keys>>,
    pub input_mapping: Rc<RefCell<InputMapping>>,
    debug: stdweb::web::Element,
    ctx: stdweb::Value,
    tool_select: Rc<RefCell<Multiselect<Tool>>>,
    tile_select: Rc<RefCell<Multiselect<tile::Style>>>,
    slope_select: Rc<RefCell<Multiselect<tile::Slope>>>,
    deadly: web::Element,
    liquid: web::Element,
    noclip: web::Element,
    friction: web::Element,
}

impl UI {
    pub fn new() -> UI {
        stdweb::initialize();
        connect::init();

        let document = web::document();

        let debug = document.get_element_by_id("debug").unwrap();
        let ctx = js! {
            let area = document.getElementById("canvasarea");
            let cvs = document.getElementById("gamecanvas");
            let ctx = cvs.getContext("2d", {alpha: false});
            ctx.imageSmoothingEnabled = false;
            const AREA = 400 * 400;
            window.onresize = e => {
                let dpr = window.devicePixelRatio || 1;
                let viewportPhysicalW = area.clientWidth * dpr;
                let viewportPhysicalH = area.clientHeight * dpr;
                let scale = Math.sqrt(viewportPhysicalW * viewportPhysicalH / AREA);
                scale = Math.floor(Math.max(2, scale));
                ctx.ducksScale = scale;
                cvs.width = viewportPhysicalW / scale;
                cvs.height = viewportPhysicalH / scale;
                cvs.style.width = (cvs.width * scale / dpr) + "px";
                cvs.style.height = (cvs.height * scale / dpr) + "px";
            };
            window.onresize();
            return ctx;
        };

        let mouse = Mouse::new(&document.get_element_by_id("gamecanvas").unwrap());
        let keys = Keys::new();
        let input_mapping = InputMapping::new(keys.clone());
        inputmapper::init(input_mapping.clone());
        js! {
            document.documentElement.oncontextmenu =
                e => e.target.tagName == "INPUT" && e.target.type == "text";
        }

        let tiles = [
            (tile::Style::Grassy, "grassy"),
            (tile::Style::Spooky, "spooky"),
            (tile::Style::Oozy, "oozy"),
            (tile::Style::Cake, "cake"),
            (tile::Style::Icey, "icey"),
            (tile::Style::Acid, "acid"),
            (tile::Style::Dark, "dark"),
            (tile::Style::Steampunk, "steampunk"),
            (tile::Style::Fiery, "fiery"),
            (tile::Style::Pipesy, "pipesy"),
            (tile::Style::Steely, "steely"),
            (tile::Style::Sandstone, "sandstone"),
            (tile::Style::Cloudy, "cloudy"),
            (tile::Style::Bane, "bane"),
            (tile::Style::BubbleCloudy, "bubblecloudy"),
            (tile::Style::Cirrusly, "cirrusly"),
            (tile::Style::HgGraph, "hggraph"),
            (tile::Style::Rainbow, "rainbow"),
            (tile::Style::Numbers, "numbers"),
            (tile::Style::LoveThyNeighbor, "lovethyneighbor"),
            (tile::Style::Cow, "cow"),
            (tile::Style::Lego, "lego"),
            (tile::Style::Sandy, "sandy"),
            (tile::Style::Templatey, "templatey"),
        ];
        let tile_select =
            Multiselect::new("toolright1", tile::Style::Grassy, &tiles, |elem, name| {
                js! {
                    let elem = @{elem};
                    let name = @{name};
                    elem.type = "image";
                    elem.src = "img/tiles/" + name + ".png";
                    elem.alt = name;
                }
            });

        let tools = [(Tool::Pencil, "pencil"), (Tool::Rectangle, "rectangle")];
        let tool_select = Multiselect::new("toolright2", Tool::Pencil, &tools, |elem, name| {
            js! {
                let elem = @{elem};
                let name = @{name};
                elem.type = "image";
                elem.src = "img/" + name + ".png";
                elem.alt = name;
            }
        });

        let slopes = [
            (tile::Slope::BottomUp, "◢"),
            (tile::Slope::BottomDown, "◣"),
            (tile::Slope::None, "◼"),
            (tile::Slope::TopDown, "◥"),
            (tile::Slope::TopUp, "◤"),
        ];
        let slope_select =
            Multiselect::new("toolright4", tile::Slope::None, &slopes, |elem, name| {
                js! {
                    let elem = @{elem};
                    elem.type = "button";
                    elem.value = @{name};
                    elem.style.fontSize = "32px";
                    elem.style.lineHeight = "0";
                }
            });

        let friction = document.get_element_by_id("friction").unwrap();
        friction.add_event_listener(move |e: web::event::ChangeEvent| {
            js! {
                let el = @{e.target()};
                el.value = @{parse_number_field}(el.value);
            }
        });

        UI {
            mouse,
            keys,
            input_mapping,
            debug,
            ctx,
            tile_select,
            tool_select,
            slope_select,
            deadly: document.get_element_by_id("deadly").unwrap(),
            liquid: document.get_element_by_id("liquid").unwrap(),
            noclip: document.get_element_by_id("noclip").unwrap(),
            friction,
        }
    }

    pub fn pre(&mut self, grid: &mut Grid) {
        drawing::pre(&self, grid);
    }
    pub fn draw(&mut self, grid: &Grid) {
        let s = format!("{:#?}", self);
        //js! {
        //    @{&self.debug}.innerText = @{s};
        //}

        js! {
            let ctx = @{&self.ctx};
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        }
        grid.draw(&self.ctx);
    }
    pub fn post(&mut self) {
        self.mouse.borrow_mut().post();
        self.keys.borrow_mut().post();
    }

    pub fn scale(&self) -> i32 {
        (js! {
            return @{&self.ctx}.ducksScale;
        }).try_into()
            .unwrap()
    }

    pub fn tool(&self) -> Tool {
        self.tool_select.borrow().active
    }
    pub fn tile(&self) -> tile::Style {
        self.tile_select.borrow().active
    }
    pub fn slope(&self) -> tile::Slope {
        self.slope_select.borrow().active
    }

    // TODO?: optimize these by using events to save the state into a rust var
    pub fn deadly(&self) -> bool {
        if let Value::Bool(b) = js! { return @{&self.deadly}.checked; } {
            b
        } else {
            unreachable!();
        }
    }
    pub fn liquid(&self) -> bool {
        if let Value::Bool(b) = js! { return @{&self.liquid}.checked; } {
            b
        } else {
            unreachable!();
        }
    }
    pub fn noclip(&self) -> bool {
        if let Value::Bool(b) = js! { return @{&self.noclip}.checked; } {
            b
        } else {
            unreachable!();
        }
    }
    pub fn friction(&self) -> f32 {
        parse_number_field(js! { return @{&self.friction}.value; })
    }
}

impl fmt::Debug for UI {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "UI(")?;
        write!(f, "tile: {:#?}, ", self.tile())?;
        write!(f, "tool: {:#?}, ", self.tool())?;
        write!(f, "slope: {:#?}, ", self.slope())?;
        write!(f, "deadly: {:#?}, ", self.deadly())?;
        write!(f, "liquid: {:#?}, ", self.liquid())?;
        write!(f, "noclip: {:#?}, ", self.noclip())?;
        write!(f, "friction: {:#?})", self.friction())?;
        Ok(())
    }
}

fn parse_number_field(v: Value) -> f32 {
    v.into_string().unwrap().parse().unwrap_or(1f32)
}
