use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
use super::Keys;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum Action {
    Up,
    Down,
    Left,
    Right,
    Jump,
}

impl Action {
    pub fn name(&self) -> &'static str {
        match *self {
            Action::Up => "Up",
            Action::Down => "Down",
            Action::Left => "Left",
            Action::Right => "Right",
            Action::Jump => "Jump",
        }
    }
}

pub struct InputMapping {
    keys: Rc<RefCell<Keys>>,
    pub map: HashMap<Action, String>,
}

impl InputMapping {
    pub fn new(keys: Rc<RefCell<Keys>>) -> Rc<RefCell<InputMapping>> {
        let mut map: HashMap<Action, String> = HashMap::new();
        map.insert(Action::Up, "ArrowUp".into());
        map.insert(Action::Down, "ArrowDown".into());
        map.insert(Action::Left, "ArrowLeft".into());
        map.insert(Action::Right, "ArrowRight".into());
        map.insert(Action::Jump, "KeyZ".into());

        Rc::new(RefCell::new(InputMapping {
            keys,
            map,
        }))
    }

    pub fn pressed(&self, action: Action) -> bool {
        self.keys.borrow().pressed(&self.map[&action])
    }

    pub fn pressed_this_frame(&self, action: Action) -> bool {
        self.keys.borrow().pressed_this_frame(&self.map[&action])
    }

    pub fn axis(&self, neg: Action, pos: Action) -> i8 {
        let mut val = 0;
        if self.pressed(neg) {
            val -= 1;
        }
        if self.pressed(pos) {
            val += 1;
        }
        val
    }
}
