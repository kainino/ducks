use std::rc::Rc;
use std::cell::RefCell;

use stdweb::web;
use stdweb::web::{document, IEventTarget, INode};
use stdweb::web::INonElementParentNode;

pub struct Multiselect<E: 'static + Eq + Copy> {
    pub active: E,
    options: Vec<(E, web::Element)>,
}

impl<E: Eq + Copy> Multiselect<E> {
    pub fn new<F>(
        parentid: &str,
        active: E,
        options: &[(E, &str)],
        applicator: F,
    ) -> Rc<RefCell<Multiselect<E>>>
    where
        F: Fn(&web::Element, &str),
    {
        let this = Rc::new(RefCell::new(Multiselect {
            active: active,
            options: vec![],
        }));
        let parent = document().get_element_by_id(parentid).unwrap();
        let options = options
            .iter()
            .map(|&option| {
                let e = option.0;
                let is_active = e == active;
                let mut elem = document().create_element("input").unwrap();
                parent.append_child(&elem);
                js! {
                    let elem = @{&elem};
                    elem.className = @{is_active} ? "ms msactive" : "ms";
                    elem.draggable = false;
                }
                applicator(&mut elem, option.1);
                let this = this.clone();
                elem.add_event_listener(move |_: web::event::ClickEvent| {
                    this.borrow_mut().set_active(e);
                });
                (e, elem)
            })
            .collect();
        this.borrow_mut().options = options;
        this
    }

    fn set_active(&mut self, active: E) {
        self.active = active;
        for o in &self.options {
            js! {
                @{&o.1}.className = @{o.0 == active} ? "ms msactive" : "ms";
            }
        }
    }
}
