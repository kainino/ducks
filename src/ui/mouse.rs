use std::cell::RefCell;
use std::rc::Rc;
use stdweb::web;

#[derive(Debug)]
pub struct Buttons(i32);

impl Buttons {
    pub fn left(&self) -> bool {
        (self.0 & 0x1) != 0
    }
    pub fn right(&self) -> bool {
        (self.0 & 0x2) != 0
    }
    pub fn middle(&self) -> bool {
        (self.0 & 0x4) != 0
    }
}

#[derive(Debug)]
pub struct Mouse {
    pub over: bool,
    pub x: i32,
    pub y: i32,
    pub device_pixel_ratio: f64,
    pub scroll: f64,
    pub buttons: Buttons,
    pub new_buttons: Buttons,
}

impl Mouse {
    pub fn new(mouse_target: &web::Element) -> Rc<RefCell<Mouse>> {
        let this = Rc::new(RefCell::new(Mouse {
            over: false,
            scroll: 0f64,
            x: 0,
            y: 0,
            device_pixel_ratio: 1.0,
            buttons: Buttons(0),
            new_buttons: Buttons(0),
        }));
        let on_leave = {
            let this = this.clone();
            move || {
                this.borrow_mut().over = false;
            }
        };
        let on_wheel = {
            let this = this.clone();
            move |delta_y: f64| {
                this.borrow_mut().scroll += delta_y;
            }
        };
        let on_have_position = {
            let this = this.clone();
            move |dpr: f64, x: i32, y: i32| {
                let mut this = this.borrow_mut();
                this.device_pixel_ratio = dpr;
                this.x = x;
                this.y = y;
                this.over = true;
            }
        };
        let on_button_change = {
            let this = this.clone();
            move |buttons: i32, allow_new: bool| {
                let mut this = this.borrow_mut();
                if allow_new {
                    this.new_buttons.0 |= buttons & !this.buttons.0;
                }
                this.buttons.0 = buttons;
            }
        };
        js! {
            let tgt = @{&mouse_target};
            tgt.onmouseleave = e => @{on_leave}();
            tgt.onwheel = e => @{on_wheel}(e.deltaY);
            let onButtonChange = @{on_button_change};
            let onHavePosition = e => {
                let x = e.clientX + document.documentElement.scrollLeft - tgt.offsetLeft;
                let y = e.clientY + document.documentElement.scrollTop - tgt.offsetTop;
                @{on_have_position}(window.devicePixelRatio, x, y);
            };
            // TODO: handle touch events?
            tgt.onmousemove = e => {
                onHavePosition(e);
            };
            tgt.onmousedown = e => {
                onHavePosition(e);
                onButtonChange(e.buttons, true);
            };
            document.onmouseup = e => {
                onButtonChange(e.buttons, false);
            };
        }
        this
    }

    pub fn post(&mut self) {
        //let s = format!("{:?}", self);
        //js! { console.log(@{s}); }

        self.scroll = 0f64;
        self.new_buttons = Buttons(0);
    }

    pub fn scaled_x(&self) -> i32 {
        (self.x as f64 * self.device_pixel_ratio) as i32
    }
    pub fn scaled_y(&self) -> i32 {
        (self.y as f64 * self.device_pixel_ratio) as i32
    }
}
