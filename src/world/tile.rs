#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Style {
    Grassy,
    Spooky,
    Oozy,
    Cake,
    Icey,
    Acid,
    Dark,
    Steampunk,
    Fiery,
    Pipesy,
    Steely,
    Sandstone,
    Cloudy,
    Bane,
    BubbleCloudy,
    Cirrusly,
    HgGraph,
    Rainbow,
    Numbers,
    LoveThyNeighbor,
    Cow,
    Lego,
    Sandy,
    Templatey,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Slope {
    None,
    TopDown,
    TopUp,
    BottomUp,
    BottomDown,
}

#[derive(Clone, Copy, Debug)]
pub struct Tile {
    pub style: Style,
    pub slope: Slope,
    pub deadly: bool,
    pub liquid: bool,
    pub noclip: bool,
    pub friction: f32,
}
