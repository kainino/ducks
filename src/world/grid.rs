use std::collections::HashMap;
use stdweb;
use sprite::Sprite;
use sprite::tile::*;
use world::tile::*;

pub struct Grid {
    tiles: HashMap<(i32, i32), Tile>,
    tilesets: Tilesets,
}

impl Grid {
    pub fn new() -> Grid {
        Grid {
            tiles: HashMap::new(),
            tilesets: make_tilesets(),
        }
    }

    pub fn set(&mut self, x: i32, y: i32, tile: Option<Tile>) {
        if let Some(tile) = tile {
            self.tiles.insert((x, y), tile);
        } else {
            self.tiles.remove(&(x, y));
        }
    }

    fn neighbors(&self, x: i32, y: i32, tile: &Tile) -> u8 {
        let mut neighbors = 0u8;
        for o in OFFSETS.iter() {
            let n: u8 = if let Some(t) = self.tiles.get(&(x + o.0, y + o.1)) {
                // TODO: logic to handle slopes
                if t.style == tile.style {
                    1
                } else {
                    0
                }
            } else {
                0
            };
            neighbors = (neighbors << 1) | n;
        }
        neighbors
    }
}

const OFFSETS: [(i32, i32); 8] = [
    (-1, -1),
    (0, -1),
    (1, -1),
    (1, 0),
    (1, 1),
    (0, 1),
    (-1, 1),
    (-1, 0),
];

impl ::rendering::Drawable for Grid {
    fn draw(&self, ctx: &stdweb::Value) {
        for (&(x, y), tile) in &self.tiles {
            let neighbors = self.neighbors(x, y, &tile);
            let tileset = &self.tilesets[&tile.style];
            let state = tileset.layout.state(tile.slope, neighbors);
            let offset = state.frames[0];
            let dim = tileset.layout.dim();
            js! {
                let ctx = @{ctx};
                let bmp = @{&tileset.image}.bitmap;
                if (bmp) {
                    ctx.drawImage(
                        bmp, @{offset.0}, @{offset.1}, @{dim.0}, @{dim.1},
                        @{x} * @{dim.0}, @{y} * @{dim.1}, @{dim.0}, @{dim.1});
                } else {
                    ctx.fillStyle = "#f0f";
                    ctx.fillRect(@{x} * 10, @{y} * 10, 10, 10);
                }
            }
        }
    }
}
