pub mod tile;

pub trait Sprite {
    fn dim(&self) -> (u32, u32);
}
pub type Frames = [(u32, u32)];
