use stdweb;

use super::Layout;

pub struct Tileset {
    pub layout: &'static Layout,
    pub image: stdweb::Value,
}

impl Tileset {
    pub fn new(layout: &'static Layout, path: &str) -> Tileset {
        let image = js! {
            let image = { bitmap: null };
            let img = new Image();
            new Promise(resolve => { img.onload = () => resolve(img); })
                .then(createImageBitmap)
                .then(ib => {
                    image.bitmap = ib;
                });
            img.src = @{path};
            return image;
        };
        Tileset { layout, image }
    }
}
