use super::{State, StateList};
use sprite::Sprite;
use world::tile::Slope;

pub struct Layout {
    pub no_slope: &'static StateList,
    pub top_down: &'static StateList,
    pub top_up: &'static StateList,
    pub bottom_up: &'static StateList,
    pub bottom_down: &'static StateList,
}

impl Layout {
    fn state_list(&self, slope: Slope) -> &'static StateList {
        match slope {
            Slope::None => self.no_slope,
            Slope::TopDown => self.top_down,
            Slope::TopUp => self.top_up,
            Slope::BottomUp => self.bottom_up,
            Slope::BottomDown => self.bottom_down,
        }
    }

    pub fn state(&self, slope: Slope, neighbors: u8) -> &'static State {
        for st in self.state_list(slope) {
            if st.matches(neighbors) {
                return st;
            }
        }
        unreachable!();
    }
}

impl Sprite for Layout {
    fn dim(&self) -> (u32, u32) {
        (10, 10)
    }
}
