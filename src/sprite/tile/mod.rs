#![cfg_attr(rustfmt, rustfmt_skip)]

mod state;
mod layout;
mod tileset;
pub use self::state::*;
pub use self::layout::*;
pub use self::tileset::*;

use std::collections::HashMap;
use world::tile;

// TODO: optimize by making this an array
pub type Tilesets = HashMap<tile::Style, Tileset>;

pub fn make_tilesets() -> Tilesets {
    let mut tilesets = HashMap::new();
    tilesets.insert(tile::Style::Acid,            Tileset::new(&THIRTY_TWO,  "sprites/tiles/acid.png"));
    tilesets.insert(tile::Style::Bane,            Tileset::new(&THIRTY_TWO,  "sprites/tiles/bane.png"));
    tilesets.insert(tile::Style::BubbleCloudy,    Tileset::new(&THIRTY_TWO,  "sprites/tiles/bubblecloudy.png"));
    tilesets.insert(tile::Style::Cake,            Tileset::new(&THIRTY_TWO,  "sprites/tiles/cake.png"));
    tilesets.insert(tile::Style::Cirrusly,        Tileset::new(&THIRTY_TWO,  "sprites/tiles/cirrusly.png"));
    tilesets.insert(tile::Style::Cloudy,          Tileset::new(&THIRTY_TWO,  "sprites/tiles/cloudy.png"));
    tilesets.insert(tile::Style::Cow,             Tileset::new(&THIRTY_TWO,  "sprites/tiles/cow.png"));
    tilesets.insert(tile::Style::Dark,            Tileset::new(&THIRTY_TWO,  "sprites/tiles/dark.png"));
    tilesets.insert(tile::Style::Fiery,           Tileset::new(&THIRTY_TWO,  "sprites/tiles/fiery.png"));
    tilesets.insert(tile::Style::Grassy,          Tileset::new(&THIRTY_TWO,  "sprites/tiles/grassy.png"));
    tilesets.insert(tile::Style::HgGraph,         Tileset::new(&THIRTY_TWO,  "sprites/tiles/hggraph.png"));
    tilesets.insert(tile::Style::Icey,            Tileset::new(&THIRTY_TWO,  "sprites/tiles/icey.png"));
    tilesets.insert(tile::Style::Lego,            Tileset::new(&THIRTY_TWO,  "sprites/tiles/lego.png"));
    tilesets.insert(tile::Style::LoveThyNeighbor, Tileset::new(&THIRTY_TWO,  "sprites/tiles/lovethyneighbor.png"));
    tilesets.insert(tile::Style::Numbers,         Tileset::new(&THIRTY_TWO,  "sprites/tiles/numbers.png"));
    tilesets.insert(tile::Style::Oozy,            Tileset::new(&THIRTY_TWO,  "sprites/tiles/oozy.png"));
    tilesets.insert(tile::Style::Pipesy,          Tileset::new(&THIRTY_TWO,  "sprites/tiles/pipesy.png"));
    tilesets.insert(tile::Style::Rainbow,         Tileset::new(&THIRTY_TWO,  "sprites/tiles/rainbow.png"));
    tilesets.insert(tile::Style::Sandstone,       Tileset::new(&THIRTY_TWO,  "sprites/tiles/sandstone.png"));
    tilesets.insert(tile::Style::Sandy,           Tileset::new(&SIXTY_SEVEN, "sprites/tiles/sandy.png"));
    tilesets.insert(tile::Style::Spooky,          Tileset::new(&THIRTY_TWO,  "sprites/tiles/spooky.png"));
    tilesets.insert(tile::Style::Steampunk,       Tileset::new(&THIRTY_TWO,  "sprites/tiles/steampunk.png"));
    tilesets.insert(tile::Style::Steely,          Tileset::new(&THIRTY_TWO,  "sprites/tiles/steely.png"));
    tilesets.insert(tile::Style::Templatey,       Tileset::new(&SIXTY_SEVEN, "sprites/tiles/templatey.png"));
    tilesets
}

pub const THIRTY_TWO: Layout = Layout {
    no_slope: &[
        State { docare: 0b01010101, filled: 0b00010100, frames: &[(  0,   0)] },
        State { docare: 0b01010101, filled: 0b01010100, frames: &[(  0,  11)] },
        State { docare: 0b01010101, filled: 0b01010000, frames: &[(  0,  22)] },
        State { docare: 0b01010101, filled: 0b00010000, frames: &[(  0,  33)] },

        State { docare: 0b01010101, filled: 0b00010101, frames: &[( 11,   0)] },
        State { docare: 0b01010101, filled: 0b01010101, frames: &[( 11,  11)] },
        State { docare: 0b01010101, filled: 0b01010001, frames: &[( 11,  22)] },
        State { docare: 0b01010101, filled: 0b00010001, frames: &[( 11,  33)] },

        State { docare: 0b01010101, filled: 0b00000101, frames: &[( 22,   0)] },
        State { docare: 0b01010101, filled: 0b01000101, frames: &[( 22,  11)] },
        State { docare: 0b01010101, filled: 0b01000001, frames: &[( 22,  22)] },
        State { docare: 0b01010101, filled: 0b00000001, frames: &[( 22,  33)] },

        State { docare: 0b01010101, filled: 0b00000100, frames: &[( 33,   0)] },
        State { docare: 0b01010101, filled: 0b01000100, frames: &[( 33,  11)] },
        State { docare: 0b01010101, filled: 0b01000000, frames: &[( 33,  22)] },
        State { docare: 0b01010101, filled: 0b00000000, frames: &[( 33,  33)] },
    ],
    bottom_down: &[
        State { docare: 0b00000101, filled: 0b00000101, frames: &[( 44,  22)] },
        State { docare: 0b00000101, filled: 0b00000100, frames: &[( 55,  22)] },
        State { docare: 0b00000101, filled: 0b00000001, frames: &[( 66,  22)] },
        State { docare: 0b00000101, filled: 0b00000000, frames: &[( 77,  22)] },
    ],
    bottom_up: &[
        State { docare: 0b00010100, filled: 0b00010100, frames: &[( 44,   0)] },
        State { docare: 0b00010100, filled: 0b00000100, frames: &[( 55,   0)] },
        State { docare: 0b00010100, filled: 0b00010000, frames: &[( 66,   0)] },
        State { docare: 0b00010100, filled: 0b00000000, frames: &[( 77,   0)] },
    ],
    top_up: &[
        State { docare: 0b10000001, filled: 0b10000001, frames: &[( 44,  33)] },
        State { docare: 0b10000001, filled: 0b10000000, frames: &[( 55,  33)] },
        State { docare: 0b10000001, filled: 0b00000001, frames: &[( 66,  33)] },
        State { docare: 0b10000001, filled: 0b00000000, frames: &[( 77,  33)] },
    ],
    top_down: &[
        State { docare: 0b10100000, filled: 0b10100000, frames: &[( 44,  11)] },
        State { docare: 0b10100000, filled: 0b10000000, frames: &[( 55,  11)] },
        State { docare: 0b10100000, filled: 0b00100000, frames: &[( 66,  11)] },
        State { docare: 0b10100000, filled: 0b00000000, frames: &[( 77,  11)] },
    ],
};

pub const SIXTY_SEVEN: Layout = Layout {
    no_slope: &[
        State { docare: 0b01010101, filled: 0b00000001, frames: &[(  0,   0)] },
        State { docare: 0b01010101, filled: 0b01000000, frames: &[( 11,   0)] },
        State { docare: 0b01010101, filled: 0b00010000, frames: &[( 22,   0)] },
        State { docare: 0b01010101, filled: 0b00000100, frames: &[( 33,   0)] },

        State { docare: 0b01010111, filled: 0b00000101, frames: &[(  0,  11)] },
        State { docare: 0b11010101, filled: 0b01000001, frames: &[( 11,  11)] },
        State { docare: 0b01110101, filled: 0b01010000, frames: &[( 22,  11)] },
        State { docare: 0b01011101, filled: 0b00010100, frames: &[( 33,  11)] },

        State { docare: 0b01010111, filled: 0b00000111, frames: &[(  0,  22)] },
        State { docare: 0b11010101, filled: 0b11000001, frames: &[( 11,  22)] },
        State { docare: 0b01110101, filled: 0b01110000, frames: &[( 22,  22)] },
        State { docare: 0b01011101, filled: 0b00011100, frames: &[( 33,  22)] },

        State { docare: 0b11010111, filled: 0b01000101, frames: &[(  0,  33)] },
        State { docare: 0b11110101, filled: 0b01010001, frames: &[( 11,  33)] },
        State { docare: 0b01111101, filled: 0b01010100, frames: &[( 22,  33)] },
        State { docare: 0b01011111, filled: 0b00010101, frames: &[( 33,  33)] },

        State { docare: 0b11010111, filled: 0b01000111, frames: &[(  0,  44)] },
        State { docare: 0b11110101, filled: 0b11010001, frames: &[( 11,  44)] },
        State { docare: 0b01111101, filled: 0b01110100, frames: &[( 22,  44)] },
        State { docare: 0b01011111, filled: 0b00011101, frames: &[( 33,  44)] },

        State { docare: 0b11010111, filled: 0b11000101, frames: &[(  0,  55)] },
        State { docare: 0b11110101, filled: 0b01110001, frames: &[( 11,  55)] },
        State { docare: 0b01111101, filled: 0b01011100, frames: &[( 22,  55)] },
        State { docare: 0b01011111, filled: 0b00010111, frames: &[( 33,  55)] },

        State { docare: 0b11010111, filled: 0b11000111, frames: &[(  0,  66)] },
        State { docare: 0b11110101, filled: 0b11110001, frames: &[( 11,  66)] },
        State { docare: 0b01111101, filled: 0b01111100, frames: &[( 22,  66)] },
        State { docare: 0b01011111, filled: 0b00011111, frames: &[( 33,  66)] },

        State { docare: 0b11111111, filled: 0b11010101, frames: &[( 44,   0)] },
        State { docare: 0b11111111, filled: 0b01110101, frames: &[( 55,   0)] },
        State { docare: 0b11111111, filled: 0b01011101, frames: &[( 66,   0)] },
        State { docare: 0b11111111, filled: 0b01010111, frames: &[( 77,   0)] },

        State { docare: 0b11111111, filled: 0b11110101, frames: &[( 44,  11)] },
        State { docare: 0b11111111, filled: 0b01111101, frames: &[( 55,  11)] },
        State { docare: 0b11111111, filled: 0b01011111, frames: &[( 66,  11)] },
        State { docare: 0b11111111, filled: 0b11010111, frames: &[( 77,  11)] },

        State { docare: 0b11111111, filled: 0b11110111, frames: &[( 44,  22)] },
        State { docare: 0b11111111, filled: 0b11111101, frames: &[( 55,  22)] },
        State { docare: 0b11111111, filled: 0b01111111, frames: &[( 66,  22)] },
        State { docare: 0b11111111, filled: 0b11011111, frames: &[( 77,  22)] },

        State { docare: 0b11111111, filled: 0b01010101, frames: &[( 44,  33)] },
        State { docare: 0b01010101, filled: 0b00000000, frames: &[( 55,  33)] },
        State { docare: 0b11111111, filled: 0b01110111, frames: &[( 66,  33)] },
        State { docare: 0b11111111, filled: 0b11011101, frames: &[( 77,  33)] },

        State { docare: 0b01010101, filled: 0b00010001, frames: &[( 44,  44)] },
        State { docare: 0b01010101, filled: 0b01000100, frames: &[( 55,  44)] },
        State { docare: 0b11111111, filled: 0b11111111, frames: &[( 66,  44)] },
    ],
    bottom_down: &[
        State { docare: 0b00000111, filled: 0b00000101, frames: &[( 88,   0)] },
        State { docare: 0b00000111, filled: 0b00000111, frames: &[( 88,  11)] },
        State { docare: 0b00000101, filled: 0b00000001, frames: &[( 88,  22)] },
        State { docare: 0b00000101, filled: 0b00000100, frames: &[( 88,  33)] },
        State { docare: 0b00000101, filled: 0b00000000, frames: &[( 88,  44)] },
    ],
    bottom_up: &[
        State { docare: 0b00011100, filled: 0b00010100, frames: &[( 99,   0)] },
        State { docare: 0b00011100, filled: 0b00011100, frames: &[( 99,  11)] },
        State { docare: 0b00010100, filled: 0b00010000, frames: &[( 99,  22)] },
        State { docare: 0b00010100, filled: 0b00000100, frames: &[( 99,  33)] },
        State { docare: 0b00010100, filled: 0b00000000, frames: &[( 99,  44)] },
    ],
    top_up: &[
        State { docare: 0b11000001, filled: 0b01000001, frames: &[(110,   0)] },
        State { docare: 0b11000001, filled: 0b11000001, frames: &[(110,  11)] },
        State { docare: 0b01000001, filled: 0b00000001, frames: &[(110,  22)] },
        State { docare: 0b01000001, filled: 0b01000000, frames: &[(110,  33)] },
        State { docare: 0b01000001, filled: 0b00000000, frames: &[(110,  44)] },
    ],
    top_down: &[
        State { docare: 0b01110000, filled: 0b01010000, frames: &[(121,   0)] },
        State { docare: 0b01110000, filled: 0b01110000, frames: &[(121,  11)] },
        State { docare: 0b01010000, filled: 0b00010000, frames: &[(121,  22)] },
        State { docare: 0b01010000, filled: 0b01000000, frames: &[(121,  33)] },
        State { docare: 0b01010000, filled: 0b00000000, frames: &[(121,  44)] },
    ],
};
