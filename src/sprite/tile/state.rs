use sprite;

pub struct State {
    // A bit is set if we care if that neighbor matches.
    pub docare: u8,
    // A bit is set if that neighbor needs to be "connected" (if we care).
    pub filled: u8,
    // List of animation frames for this state
    pub frames: &'static sprite::Frames,
}

impl State {
    pub fn matches(&self, neighbors: u8) -> bool {
        (neighbors ^ self.filled) & self.docare == 0
    }
}
pub type StateList = [State];
