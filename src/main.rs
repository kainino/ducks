#![recursion_limit = "256"]

#[macro_use]
extern crate stdweb;

mod ui;
mod world;
mod sprite;
mod rendering;

pub struct Main {
    pub grid: world::Grid,
    ui: ui::UI,
}

impl Main {
    fn new() -> Main {
        let ui = ui::UI::new();
        let grid = world::Grid::new();
        Main { grid, ui }
    }

    fn frame(&mut self) {
        self.ui.pre(&mut self.grid);
        self.ui.draw(&self.grid);
        self.ui.post();
    }
}

fn main() {
    let mut main = Main::new();

    js! {
        let raf = requestAnimationFrame;
        //let raf = f => setTimeout(f, 1000);
        raf(function frame() {
            raf(frame);
            @{move || main.frame()}();
        });
    }
}
