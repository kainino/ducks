.PHONY: build
build:
	cargo web build --target-webasm
	mkdir -p build/js/
	cp target/wasm32-unknown-unknown/release/ducks.wasm build/ducks.wasm
	cp target/wasm32-unknown-unknown/release/ducks.js build/js/app.js
	rsync -a static/* build/
